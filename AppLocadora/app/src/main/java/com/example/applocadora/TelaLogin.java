package com.example.applocadora;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.applocadora.ConfigFirebase.ConfigAutenticacao;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class TelaLogin extends AppCompatActivity {

    private Button registrar, login;
    private String email, senha;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_login);

        verificarSeOUsusrioEstaLogado();

         final EditText emailEdit = findViewById(R.id.edit_valor_usuario);
         final EditText senhaEdit = findViewById(R.id.edit_valor_senha);



        login = findViewById(R.id.button_loginInicio);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = String.valueOf(emailEdit.getText());
                senha = String.valueOf(senhaEdit.getText());

                firebaseAuth = ConfigAutenticacao.getFirebaseAutenticacao();
                firebaseAuth.signInWithEmailAndPassword(email, senha).addOnCompleteListener(TelaLogin.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            startActivity(new Intent(TelaLogin.this, TelaPrincipal.class));
                        }else{
                            Toast.makeText(getApplicationContext(),"Email ou senha incorretos", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
        registrar = (Button) findViewById(R.id.button_registroInicio);
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TelaLogin.this, TelaRegistro.class));
            }
        });

    }

    private void verificarSeOUsusrioEstaLogado() {
            firebaseAuth = ConfigAutenticacao.getFirebaseAutenticacao();
            if (firebaseAuth.getCurrentUser() != null){
                //redirecionar para tela principal
            }
    }
}
