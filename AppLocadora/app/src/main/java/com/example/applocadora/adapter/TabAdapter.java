package com.example.applocadora.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.applocadora.Fragments.LocadorasFragment;
import com.example.applocadora.Fragments.UsuariosFragment;

public class TabAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private String[] abas = new String[]{"HOME", "USUÁRIOS"};

    public TabAdapter(@NonNull FragmentManager fm, Context c) {
        super(fm);
        this.context = c;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = new LocadorasFragment();
                break;

            case 1:
                fragment = new UsuariosFragment();
                break;
        }
        return fragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return abas[position];
    }

    @Override
    public int getCount() {
        return abas.length;
    }
}
