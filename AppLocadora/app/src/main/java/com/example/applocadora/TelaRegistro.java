package com.example.applocadora;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.applocadora.ConfigFirebase.ConfigAutenticacao;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;


public class TelaRegistro extends AppCompatActivity {

    private Usuario usuario = new Usuario();
    private Button registrar;

    private FirebaseAuth autenticacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_registro);

        //-------------Salvadno EditText-------------------//
        final EditText nome = findViewById(R.id.edit_nome);
        final EditText cnh = (findViewById(R.id.edit_cnh));
        final EditText cpf =findViewById(R.id.edit_cpf);
        final EditText sexo =(findViewById(R.id.edit_sexo));
        final EditText endereco = findViewById(R.id.edit_endereco);
        final EditText datNasc =(findViewById(R.id.edit_dat_nascimento));
        final EditText telefone =findViewById(R.id.edit_telefone);
        final EditText login =(findViewById(R.id.edit_usuarioR));
        final EditText senha = (findViewById(R.id.edit_senhaR));
        this.registrar = findViewById(R.id.button_registrar);

        //------------------Colocando mascaras------------------------------//
        SimpleMaskFormatter data = new SimpleMaskFormatter( "NN/NN/NNNN" );
        MaskTextWatcher mascaraData = new MaskTextWatcher(datNasc, data);
        datNasc.addTextChangedListener(mascaraData);

        SimpleMaskFormatter TELEFONE = new SimpleMaskFormatter( "(NN) N NNNN-NNNN" );
        MaskTextWatcher mascaraTELEFONE = new MaskTextWatcher(telefone, TELEFONE);
        telefone.addTextChangedListener(mascaraTELEFONE);

        SimpleMaskFormatter CPF = new SimpleMaskFormatter( "NNN.NNN.NNN-NN" );
        MaskTextWatcher mascaracCPF = new MaskTextWatcher(cpf, CPF);
        cpf.addTextChangedListener(mascaracCPF);

        SimpleMaskFormatter CNH = new SimpleMaskFormatter( "NNNNNNNNNNN" );
        MaskTextWatcher mascaraCNH = new MaskTextWatcher(cnh, CNH);
        cnh.addTextChangedListener(mascaraCNH);

        //----------Button Registro------------------------------//
        this.registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario.setNome(String.valueOf(nome.getText()));
                usuario.setCnh(String.valueOf(cnh.getText()));
                usuario.setCpf(String.valueOf(cpf.getText()));
                usuario.setSexo(String.valueOf(sexo.getText()));
                usuario.setDataNasc(String.valueOf(datNasc.getText()));
                usuario.setTelefone(String.valueOf(telefone.getText()));
                usuario.setSenha(String.valueOf(senha.getText()));
                usuario.setLogin(String.valueOf(login.getText()));
                usuario.setEndereco(String.valueOf(endereco.getText()));
                cadastrarUsusario();

            }
        });
    }
    //------------Método Para cadastrar-----------------------//
    private void cadastrarUsusario(){
        autenticacao = ConfigAutenticacao.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                usuario.getLogin(),
                usuario.getSenha()
        ).addOnCompleteListener(TelaRegistro.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Toast.makeText(TelaRegistro.this, "Sucesso ao cadastrar usuario", Toast.LENGTH_LONG).show();
                    FirebaseUser usuarioFirebase = task.getResult().getUser();
                    usuario.setId(usuarioFirebase.getUid());
                    usuario.salvar();
                    autenticacao.signOut();
                    finish();
                }else{
                    String erro = "";
                    if (usuario.getLogin() == null){
                        erro = "Email vazio";
                    }
                    if (usuario.getSenha() == null){
                        erro = "Senha vazia";
                    }
                    try{
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e) {
                        erro = "Digite uma senha mais forte, digitos insuficientes";
                    } catch (FirebaseAuthInvalidCredentialsException e){
                        erro = "O e-mail digitado é invalido";
                    }catch (FirebaseAuthUserCollisionException e) {
                        erro = "Esse e-mail ja existe";
                    }catch (Exception e){
                        erro = "Ao cadastrar o usuario";
                    }
                    Toast.makeText(TelaRegistro.this, "Erro: " + erro, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
