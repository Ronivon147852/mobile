package com.example.applocadora;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.applocadora.ConfigFirebase.ConfigAutenticacao;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

public class CadastroLocadora extends AppCompatActivity {

    Locadora locadora = new Locadora();
    private Button escolher, salvar;
    private ImageView imagem;
    private FirebaseAuth autenticacao;

    private Uri file;
    FirebaseStorage storage;
    StorageReference storageReference;

    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro_locadora);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        //Salvando Edict text ---------------------------------------
        final EditText cnpj = findViewById(R.id.edit_cnpj);
        final EditText nome = findViewById(R.id.edit_nome);
        final EditText endereco = findViewById(R.id.edit_endereco);
        final EditText telefone = findViewById(R.id.edit_telefone);
        final EditText abertura = findViewById(R.id.edit_horarioAbertura);
        final EditText fechamento = findViewById(R.id.edit_horarioFechamento);
        final EditText email = findViewById(R.id.edit_emailLocadora);
        final EditText senha = findViewById(R.id.edit_senhaLoccadora);
        final Button voltar = findViewById(R.id.button_voltar);
        escolher = (Button) findViewById(R.id.button_selecionarImagem);
        salvar = (Button) findViewById(R.id.button_salvar);
        imagem = (ImageView) findViewById(R.id.ImagemLocadora);
        final int PICK_IMAGE_REQUEST = 71;
        //---------------------------------------------------------------



        //------------------Colocando mascaras------------------------------//
        SimpleMaskFormatter aberturaMasc = new SimpleMaskFormatter( "NN:NN" );
        MaskTextWatcher mascaraData = new MaskTextWatcher(abertura, aberturaMasc);
        abertura.addTextChangedListener(mascaraData);

        SimpleMaskFormatter TELEFONE = new SimpleMaskFormatter( "(NN) N NNNN-NNNN" );
        MaskTextWatcher mascaraTELEFONE = new MaskTextWatcher(telefone, TELEFONE);
        telefone.addTextChangedListener(mascaraTELEFONE);

        SimpleMaskFormatter fechamentoMasc = new SimpleMaskFormatter( "NN:NN" );
        MaskTextWatcher mascaracCPF = new MaskTextWatcher(fechamento, fechamentoMasc);
        fechamento.addTextChangedListener(mascaracCPF);

        SimpleMaskFormatter cnpjMasc = new SimpleMaskFormatter( "NN.NNN.NNN/NNNN-NN" );
        MaskTextWatcher mascaraCNH = new MaskTextWatcher(cnpj, cnpjMasc);
        cnpj.addTextChangedListener(mascaraCNH);

        //---------------------------------------------------------------------------------
        escolher.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                escolherImagem();
            }
        });

        this.salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locadora.setCnpj(String.valueOf(cnpj.getText()));
                locadora.setNome(String.valueOf(nome.getText()));
                locadora.setEndereco(String.valueOf(endereco.getText()));
                locadora.setTelefone(String.valueOf(telefone.getText()));
                locadora.setHorarioAbertura(String.valueOf(abertura.getText()));
                locadora.setHorarioFechamento(String.valueOf(fechamento.getText()));
                locadora.setEmail(String.valueOf(email.getText()));
                locadora.setSenha(String.valueOf(senha.getText()));
                cadastrarLocadora();
                salvar();
            }
        });
    }

    private void salvar() {
        if (file != null){
           /* final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Em progresso");
            progressDialog.show();*/

            //modificar para salvar varias igual do usuario
            StorageReference ref = storageReference.child("Imagem");
            ref.putFile(file).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(CadastroLocadora.this, "Enviado", Toast.LENGTH_SHORT).show();

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(CadastroLocadora.this, "Falha" + e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progresso = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                   // progressDialog.setMessage("Enviado " + (int) progresso + "%");
                }
            });
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    private void escolherImagem() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "selecionar foto"), PICK_IMAGE_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST){
            file = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), file);
                imagem.setImageBitmap(bitmap);
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
    }

    private void cadastrarLocadora(){
        autenticacao = ConfigAutenticacao.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                locadora.getEmail(),
                locadora.getSenha()
        ).addOnCompleteListener(CadastroLocadora.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Toast.makeText(CadastroLocadora.this, "Sucesso ao cadastrar Locadora", Toast.LENGTH_LONG).show();
                    FirebaseUser locadoraFirebase = task.getResult().getUser();
                    locadora.setId(locadoraFirebase.getUid());
                    locadora.cadastrar();
                    autenticacao.signOut();
                    finish();
                }else{
                    String erro = "";
                    try{
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e) {
                        erro = "Digite uma senha mais forte, digitos insuficientes";
                    } catch (FirebaseAuthInvalidCredentialsException e){
                        erro = "O e-mail digitado é invalido";
                    }catch (FirebaseAuthUserCollisionException e) {
                        erro = "Esse e-mail ja existe";
                    }catch (Exception e){
                        erro = "Ao cadastrar o usuario";
                    }
                    Toast.makeText(CadastroLocadora.this, "Erro: " + erro, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
