package com.example.applocadora.Fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.applocadora.ConfigFirebase.ConfigAutenticacao;
import com.example.applocadora.R;
import com.example.applocadora.Usuario;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocadorasFragment extends Fragment{

private ListView listView;
private ArrayAdapter adapter;
private ArrayList<String> locadoras;
private DatabaseReference firebase;

    public LocadorasFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        locadoras = new ArrayList<>();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        listView = (ListView) view.findViewById(R.id.lv_locadoras);

        //ERRO colocar adapter personalizado
        adapter = new ArrayAdapter(
                getActivity(),
                R.layout.fragment_home,
                locadoras

        );

       //adapter personalizado


        listView.setAdapter(adapter);
    firebase = ConfigAutenticacao.getFirebase().child("usuarios");
    firebase.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            //Limpar Lista, para não adcionar repetidamente
            locadoras.clear();

            //Listar contatos
            for (DataSnapshot dados:dataSnapshot.getChildren()){
                Usuario usuario = dados.getValue(Usuario.class);
                locadoras.add( usuario.getNome());
            }
            adapter.notifyDataSetChanged();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    });

        return view;
    }

}
